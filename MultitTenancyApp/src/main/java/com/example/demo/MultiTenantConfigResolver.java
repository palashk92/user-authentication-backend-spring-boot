package com.example.demo;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.OIDCHttpFacade;
import org.keycloak.representations.adapters.config.AdapterConfig;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MultiTenantConfigResolver implements KeycloakConfigResolver {
    private static AdapterConfig adapterConfig;

    @Override
    public KeycloakDeployment resolve(OIDCHttpFacade.Request request) {
        final Map<String, KeycloakDeployment> cache = new ConcurrentHashMap<String, KeycloakDeployment>();
        String realm = request.getHeader("realm");
        //realm = "Realm2";
        KeycloakDeployment deployment = cache.get(realm);
        if(null == deployment) {
            InputStream is = getClass().getResourceAsStream("/" + realm + "-keycloak.json");
            System.out.println(is);
            if(is == null) {
                throw new IllegalStateException("Not able to find the file");
            }
            deployment = KeycloakDeploymentBuilder.build(is);
            cache.put(realm,deployment);
        }
        return deployment;
    }

    public static void setAdapterConfig(AdapterConfig adapterConfig) {
        MultiTenantConfigResolver.adapterConfig = adapterConfig;
    }

}
